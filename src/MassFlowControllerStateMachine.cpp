static const char *RcsId = "$Header: /users/chaize/newsvn/cvsroot/Instrumentation/MassFlowController/BROOKS_5850E/src/MassFlowControllerStateMachine.cpp,v 1.1.1.1 2005-11-29 15:48:13 syldup Exp $";
//+=============================================================================
//
// file :         MassFlowControllerStateMachine.cpp
//
// description :  C++ source for the MassFlowController and its alowed. 
//                method for commands and attributes
//
// project :      TANGO Device Server
//
// $Author: syldup $
//
// $Revision: 1.1.1.1 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :     Synchrotron SOLEIL
//                L'Orme des Merisiers
//                Saint-Aubin - BP 48
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <tango.h>
#include <MassFlowController.h>
#include <MassFlowControllerClass.h>

/*====================================================================
 *	This file contains the methods to allow commands and attributes
 * read or write execution.
 *
 * If you wand to add your own code, add it between 
 * the "End/Re-Start of Generated Code" comments.
 *
 * If you want, you can also add your own methods.
 *====================================================================
 */

namespace MassFlowController_ns
{

//=================================================
//		Attributes Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_gasFlow0Percent_allowed
// 
// description : 	Read/Write allowed for gasFlow0Percent attribute.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_gasFlow0Percent_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_gasFlow0LMin_allowed
// 
// description : 	Read/Write allowed for gasFlow0LMin attribute.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_gasFlow0LMin_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_gasFlow1Percent_allowed
// 
// description : 	Read/Write allowed for gasFlow1Percent attribute.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_gasFlow1Percent_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_gasFlow1LMin_allowed
// 
// description : 	Read/Write allowed for gasFlow1LMin attribute.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_gasFlow1LMin_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_actualMode_allowed
// 
// description : 	Read/Write allowed for actualMode attribute.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_actualMode_allowed(Tango::AttReqType type)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

//=================================================
//		Commands Allowed Methods
//=================================================

//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_IndependentMode_allowed
// 
// description : 	Execution allowed for IndependentMode command.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_IndependentMode_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_MasterSlaveMode_allowed
// 
// description : 	Execution allowed for MasterSlaveMode command.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_MasterSlaveMode_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}
//+----------------------------------------------------------------------------
//
// method : 		MassFlowController::is_Off_allowed
// 
// description : 	Execution allowed for Off command.
//
//-----------------------------------------------------------------------------
bool MassFlowController::is_Off_allowed(const CORBA::Any &any)
{
		//	End of Generated Code

		//	Re-Start of Generated Code
	return true;
}

}	// namespace MassFlowController
